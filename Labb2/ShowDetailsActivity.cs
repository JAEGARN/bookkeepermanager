using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Labb2.model;

namespace Labb2
{
    [Activity(Label = "ShowDetailsActivity")]
    public class ShowDetailsActivity : Activity, ActionBar.ITabListener
    {
        int id;
        TextView inOrOut, date, des, type, account, total, totalExk, tax;
        ImageView photo;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.Activity_ShowDetails);

            this.ActionBar.NavigationMode = ActionBarNavigationMode.Tabs;
            ActionBar.SetHomeButtonEnabled(true);
            ActionBar.SetDisplayHomeAsUpEnabled(true);

            var tab2 = this.ActionBar.NewTab();
            tab2.SetText("Detail");
            tab2.SetTabListener(this);
            ActionBar.AddTab(tab2);

            var tab = this.ActionBar.NewTab();
            tab.SetText("Edit");
            tab.SetTabListener(this);
            ActionBar.AddTab(tab);

            id = Convert.ToInt32(Intent.GetStringExtra("id"));
            Console.WriteLine("H�r �r jag inne i details"+ id);
                
            Entry e = BookKeeperManager.Instance.db.GetEntry(id);
            BookKeeperManager.Instance.db.SQLdb.Close();


            photo = FindViewById<ImageView>(Resource.Id.photo_detail);
            inOrOut = FindViewById<TextView>(Resource.Id.inOrOut_detail);
            date = FindViewById<TextView>(Resource.Id.date_detail);
            des = FindViewById<TextView>(Resource.Id.des_detail);
            type = FindViewById<TextView>(Resource.Id.type_detail);
            account = FindViewById<TextView>(Resource.Id.account_detail);
            total = FindViewById<TextView>(Resource.Id.total_detail);
            totalExk = FindViewById<TextView>(Resource.Id.totalExk_detail);
            tax = FindViewById<TextView>(Resource.Id.tax_detail);

            tab.TabSelected += delegate
            {
                Intent i = new Intent(this, typeof(EntryActivity));
                i.PutExtra("edit", "yes");
                i.PutExtra("id", id.ToString());
                StartActivity(i);
            };

            if(e.PhotoString != null)
            {
                photo.SetAdjustViewBounds(true);
                int height = Resources.DisplayMetrics.HeightPixels;
                int width = photo.Width;
                var bitmap = ImageUtils.LoadAndScaleBitmap(e.PhotoString, width, height);
                photo.SetImageBitmap(bitmap);
                bitmap.Dispose();
            }
            
            inOrOut.Text = e.EntryType;
            date.Text = e.Date;
            des.Text = e.Description;
            type.Text = e.AccountType;
            account.Text = e.ToFromAccount;
            total.Text = e.TotalWithTax.ToString();
            totalExk.Text = e.TotalExTax.ToString();
            tax.Text = e.Tax + "%";
            
        }
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    Finish();
                    return true;

                default:
                    return base.OnOptionsItemSelected(item);
            }
        }

        public void OnTabReselected(ActionBar.Tab tab, FragmentTransaction ft)
        {
            
        }

        public void OnTabSelected(ActionBar.Tab tab, FragmentTransaction ft)
        {
            
        }

        public void OnTabUnselected(ActionBar.Tab tab, FragmentTransaction ft)
        {
            
        }
    }
}