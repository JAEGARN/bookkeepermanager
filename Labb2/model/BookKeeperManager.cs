using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace Labb2.model
{
    public class BookKeeperManager
    {
        
        public Database db;
        /// <summary>
        /// Instance for the Singelton Class BookKeeperManager
        /// </summary>
        private static BookKeeperManager instance;
        public static BookKeeperManager Instance {
            get
            {
                if (instance == null)
                {
                    instance = new BookKeeperManager();
                }
                return instance; 
            }
        }
        private BookKeeperManager()
        {
            db = new Database();
            if (db.GetExpenseAccounts().Count() == 0)
            {
                db.SQLdb.Close();
                AddTaxRate(new TaxRate(6));
                AddTaxRate(new TaxRate(12));
                AddTaxRate(new TaxRate(25));
                
                AddAccount(new Account("F�rs�ljning", 3000, "Income"));
                AddAccount(new Account("F�rs�ljning av tj�nster", 3040, "Income"));
                
                AddAccount(new Account("F�rbrukningsvaror och invetarier", 5400, "Expense"));
                AddAccount(new Account("�vriga egna uttag", 2013, "Expense"));
                AddAccount(new Account("Reklam och PR", 5900, "Expense"));

                AddAccount(new Account("Kassa", 1910, "Money"));
                AddAccount(new Account("F�retagskonto", 1930, "Money"));
                AddAccount(new Account("Egna ins�ttningar", 2018, "IncomeMoney"));
            }
        }
        /// <summary>
        /// Takes an Entry and passes it on to the Database Class
        /// </summary>
        /// <param name="e"></param>
        public void AddEntry(Entry e)
        {
            db.AddEntryToDB(e);
        }
        /// <summary>
        /// Takes an Account and passes it on to the Database Class
        /// </summary>
        /// <param name="a"></param>
        public void AddAccount(Account a)
        {
            db.AddAccountToDB(a);
        }
        /// <summary>
        /// Takes an TaxRate and passes it on to the Database Class
        /// </summary>
        /// <param name="t"></param>
        public void AddTaxRate(TaxRate t)
        {
            db.AddTaxToDB(t);
        }

    }
}