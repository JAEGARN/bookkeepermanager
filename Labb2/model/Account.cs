using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace Labb2.model
{   
    public class Account
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; private set; }
        public string AccountType { get; private set; }
        public string AccountName { get; private set; }
        public int AccountNumber { get; private set; }
        /// <summary>
        /// Constructor for the Class Account
        /// </summary>
        /// <param name="name"></param>
        /// <param name="number"></param>
        /// <param name="accType"></param>
        public Account(string name, int number, string accType)
        {
            AccountType = accType;
            AccountName = name;
            AccountNumber = number;
        }
        /// <summary>
        /// Empty constructor for the Class Account
        /// </summary>
        public Account()
        {

        }

    }
}