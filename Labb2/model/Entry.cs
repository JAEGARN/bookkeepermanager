using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace Labb2.model
{
    
    public class Entry
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; private set; }
        public string EntryType { get; set; }
        public string Date { get; set; }
        public string Description { get; set; }
        public string AccountType { get; set; }
        public string ToFromAccount { get; set; }
        public double TotalWithTax { get; set; }
        public string PhotoString { get; set; }
        public string Tax { get; set; }
        public double TotalExTax { get; set; }
        

        public Entry(string date, string des, string type, string tofrom, double total, string eType, string photo, string tax, double totExTax)
        {
            EntryType = eType;
            Date = date;
            Description = des;
            AccountType = type;
            ToFromAccount = tofrom;
            TotalWithTax = total;
            PhotoString = photo;
            Tax = tax;
            TotalExTax = totExTax;
        }
        public Entry(string date, string des, string type, string tofrom, double total, string eType, string tax, double totExTax)
        {
            EntryType = eType;
            Date = date;
            Description = des;
            AccountType = type;
            ToFromAccount = tofrom;
            TotalWithTax = total;
            Tax = tax;
            TotalExTax = totExTax;
            
        }
        public Entry()
        {
            
        }
    }
}