using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace Labb2.model
{   
   
    public class Database
    {
        public SQLiteConnection SQLdb;
        public string dbPath;
        /// <summary>
        /// Constructor for the Class Database
        /// </summary>
        public Database(){
            
            dbPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal)  + @"/database.db";
            
            SQLdb = new SQLiteConnection(dbPath);
            SQLdb.CreateTable<Entry>();
            SQLdb.CreateTable<Account>();
            SQLdb.CreateTable<TaxRate>();
            SQLdb.Close();
        }
        /// <summary>
        /// Adds an Entry to the Database
        /// </summary>
        /// <param name="e"></param>
        public void AddEntryToDB(Entry e)
        {
            SQLdb = new SQLiteConnection(dbPath);
            SQLdb.Insert(e);
            SQLdb.Close();
        }
        /// <summary>
        /// Adds an Account to the Database
        /// </summary>
        /// <param name="a"></param>
        public void AddAccountToDB(Account a)
        {
            SQLdb = new SQLiteConnection(dbPath);
            SQLdb.Insert(a);
            SQLdb.Close();
        }
        /// <summary>
        /// Adds an TaxRate to the Database
        /// </summary>
        /// <param name="t"></param>
        public void AddTaxToDB(TaxRate t)
        {
            SQLdb = new SQLiteConnection(dbPath);
            SQLdb.Insert(t);
            SQLdb.Close();
        }
        /// <summary>
        /// Gets all the Entries from the Database
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Entry> GetEntries()
        {
            SQLdb = new SQLiteConnection(dbPath);
            return SQLdb.Table<Entry>();
        }
        /// <summary>
        /// Gets accounts that are MoneyAccounts from the Database
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Account> GetMoneyAccounts()
        {
            SQLdb = new SQLiteConnection(dbPath);
            return SQLdb.Table<Account>().Where(a => a.AccountType.Equals("Money"));
        }
        /// <summary>
        /// Gets all accounts that are MoneyAccounts from the Database
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Account> GetAllMoneyAccounts()
        {
            SQLdb = new SQLiteConnection(dbPath);
            return SQLdb.Table<Account>().Where(a => a.AccountType.Equals("Money") || a.AccountType.Equals("IncomeMoney"));
        }
        /// <summary>
        /// Gets all accounts that are IncomeAccounts from the Database
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Account> GetIncomeAccounts()
        {
            SQLdb = new SQLiteConnection(dbPath);
            return SQLdb.Table<Account>().Where(ia => ia.AccountType.Equals("Income"));
        }
        /// <summary>
        /// Gets all accounts that are ExpenseAccounts from the Database
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Account> GetExpenseAccounts()
        {
            SQLdb = new SQLiteConnection(dbPath);
            return SQLdb.Table<Account>().Where(ea => ea.AccountType.Equals("Expense"));
        }
        /// <summary>
        /// Gets all TaxRates from the Database
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TaxRate> GetTaxes()
        {
            SQLdb = new SQLiteConnection(dbPath);
            return SQLdb.Table<TaxRate>();
        }
        /// <summary>
        /// Gets an Entry that lies on the "id" in the Database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Entry GetEntry(int id)
        {
            SQLdb = new SQLiteConnection(dbPath);
            return SQLdb.Table<Entry>().ElementAt(id);
        }

        public void UpdateEntry()
        {
            SQLdb = new SQLiteConnection(dbPath);
            SQLdb.UpdateAll(SQLdb.Table<Entry>());
            SQLdb.Close();
        }

        public void UpdateAll(Entry e)
        {
            SQLdb = new SQLiteConnection(dbPath);
            SQLdb.Execute("UPDATE Entry SET TotalWithTax = " + e.TotalWithTax + ", Description = '" + e.Description + "', EntryType = '" + e.EntryType + "', AccountType = '" + e.AccountType + "',ToFromAccount = '" + e.ToFromAccount + "', Tax = '" + e.Tax + "' WHERE Id = " + e.Id);
            //'
            SQLdb.Close();
        }

    }
}