using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Labb2.model;

namespace Labb2
{
    [Activity(Label = "All Entries", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    class AllEntriesActivity : Activity
    {
        private ListView entriesList;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
           
            SetContentView(Resource.Layout.Activity_AllEntries);
            
            entriesList = FindViewById<ListView>(Resource.Id.entries_list);
            ArrayAdapter entriesAdapter =
                    new ArrayAdapter(this, Android.Resource.Layout.SimpleListItem1,
                    BookKeeperManager.Instance.db.GetEntries().Select(el => ("Datum:  " + el.Date + "\n") + ("Beskrivning:  " + el.Description + "\n") + ("Totalt:  " + el.TotalWithTax+":-")).ToList());
                    //.OrderByDescending(e => e.Id)
            entriesAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleListItem1);
            entriesList.Adapter = entriesAdapter;
            BookKeeperManager.Instance.db.SQLdb.Close();
        
            entriesList.ItemClick += (object sender, Android.Widget.AdapterView.ItemClickEventArgs e) =>
            {
                Intent i = new Intent(this, typeof(ShowDetailsActivity));
                i.PutExtra("id", e.Id.ToString());
                StartActivity(i);
            };
        }
    }
}