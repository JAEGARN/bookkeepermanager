﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace Labb2
{
    [Activity(Label = "Labb2", MainLauncher = true, Icon = "@drawable/icon", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Activity_Main);

            // Get our button from the layout resource,
            // and attach an event to it
            Button newEntryButton = FindViewById<Button>(Resource.Id.new_entry_button);
            Button showEntriesButton = FindViewById<Button>(Resource.Id.show_entries_button);
            
            newEntryButton.Click += delegate 
            {
                Intent i = new Intent(this, typeof(EntryActivity));
                StartActivity(i);

            };
            showEntriesButton.Click += delegate 
            {
                Intent i = new Intent(this, typeof(AllEntriesActivity));
                StartActivity(i);
            };
            
        }
    }
}

