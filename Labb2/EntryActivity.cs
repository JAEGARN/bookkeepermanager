using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Labb2.model;
using Android.Provider;
using SQLite;
using Java.IO;
using AUri = Android.Net.Uri;
using AEnvi = Android.OS.Environment;
using AFile = Java.IO.File;
using Android.Graphics;
using System.Globalization;




namespace Labb2
{
    [Activity(Label = "Entry", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class EntryActivity : Activity
    {
        private bool isPhotoTaken = false;
        private string entryType;
        private double totalCashEXTax;
        private AUri myUri = null;
        private RadioButton incomeRB, expenseRB;
        private RadioGroup RBGroup;
        private Spinner type, toFrom, tax;
        private ImageView photoView;
        private TextView totalExk, total, des;
        private Button takePhotoButton, addEntrybutton;
        DatePicker datePick;
        
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            
            SetContentView(Resource.Layout.Activity_Entry);

            tax = FindViewById<Spinner>(Resource.Id.moms_spinner);
            toFrom = FindViewById<Spinner>(Resource.Id.account_spinner);
            type = FindViewById<Spinner>(Resource.Id.type_spinner);
            RBGroup = FindViewById<RadioGroup>(Resource.Id.income_expense_group);
            incomeRB = FindViewById<RadioButton>(Resource.Id.income_radio);
            expenseRB = FindViewById<RadioButton>(Resource.Id.expense_radio);
            datePick = FindViewById<DatePicker>(Resource.Id.date_pick);
            des = FindViewById<TextView>(Resource.Id.des_text);
            total = FindViewById<TextView>(Resource.Id.total_text);
            totalExk = FindViewById<TextView>(Resource.Id.total_exk_text);

            photoView = FindViewById<ImageView>(Resource.Id.photo_view);

            takePhotoButton = FindViewById<Button>(Resource.Id.take_photo_button);
            addEntrybutton = FindViewById<Button>(Resource.Id.add_entry_button);

            setTaxAdapter();

            totalExk.Text = "-insert total";

            IsItEdit();
            
            takePhotoButton.Click += delegate
            {
                TakePhoto();
            };

            tax.ItemSelected += delegate
            {
                updateAmount();
            };
            
            total.TextChanged += delegate
            {
                updateAmount();
            };

            RBGroup.CheckedChange += delegate
            {
               entryType = WhichRadioIsChecked();
            };

            addEntrybutton.Click += delegate
            {
                if (Intent.GetStringExtra("edit") == "yes")
                {
                    int id = Convert.ToInt32(Intent.GetStringExtra("id"));
                    Entry e = BookKeeperManager.Instance.db.GetEntry(id);
                    BookKeeperManager.Instance.db.SQLdb.Close();
                    
                    e.Description = des.Text;
                    e.TotalWithTax = Convert.ToDouble(total.Text);
                    e.Tax = tax.SelectedItem.ToString();
                    e.ToFromAccount = toFrom.SelectedItem.ToString();
                    e.EntryType = type.SelectedItem.ToString();
                    e.AccountType = toFrom.SelectedItem.ToString();

                    BookKeeperManager.Instance.db.UpdateAll(e);

                }
                else
                {

                    double totalCashINTax = Convert.ToDouble(total.Text.ToString());

                    if (isPhotoTaken)
                    {
                        BookKeeperManager.Instance.AddEntry(new Entry(datePick.DateTime.ToShortDateString(), des.Text, type.SelectedItem.ToString(),
                        toFrom.SelectedItem.ToString(), totalCashINTax, entryType, myUri.Path, tax.SelectedItem.ToString(), totalCashEXTax));
                    }
                    else
                    {
                        BookKeeperManager.Instance.AddEntry(new Entry(datePick.DateTime.ToShortDateString(), des.Text, type.SelectedItem.ToString(),
                        toFrom.SelectedItem.ToString(), totalCashINTax, entryType, tax.SelectedItem.ToString(), totalCashEXTax));
                    }
                }
                Intent i = new Intent(this, typeof(AllEntriesActivity));
                StartActivity(i);
            };
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            if (requestCode == 0 && resultCode == Result.Ok)
            {
                photoView.SetAdjustViewBounds(true);
                int height = Resources.DisplayMetrics.HeightPixels;
                int width = photoView.Width;
               
                //Er ImageUtils kod g�rs s� att man l�tt f�r out of Memory! �terkom om jag ska visa!!
                var bitmap = ImageUtils.LoadAndScaleBitmap(myUri.Path, width, height);
                photoView.SetImageBitmap(bitmap);
                bitmap.Dispose();
                isPhotoTaken = true;
            }
            else
            {
                base.OnActivityResult(requestCode, resultCode, data);
            }
        }
        private String WhichRadioIsChecked()
        {
            if(incomeRB.Checked)
            {   
                ArrayAdapter incomeAdapter =
                    new ArrayAdapter(this, Android.Resource.Layout.SimpleSpinnerItem,
                    BookKeeperManager.Instance.db.GetIncomeAccounts().Select(ea => ea.AccountName + "( " + ea.AccountNumber + " )").ToList());

                incomeAdapter.SetDropDownViewResource(Android.Resource.Layout.
                SimpleSpinnerDropDownItem);
                type.Adapter = incomeAdapter;

                ArrayAdapter incomeMoneyAdapter =
                   new ArrayAdapter(this, Android.Resource.Layout.SimpleSpinnerItem,
                   BookKeeperManager.Instance.db.GetAllMoneyAccounts().Select(am => am.AccountName + "( " + am.AccountNumber + " )").ToList());

                incomeMoneyAdapter.SetDropDownViewResource(Android.Resource.Layout.
                SimpleSpinnerDropDownItem);
                toFrom.Adapter = incomeMoneyAdapter;

                BookKeeperManager.Instance.db.SQLdb.Close();
            
            }else if(expenseRB.Checked)
            {
                ArrayAdapter expenseAdapter =
                    new ArrayAdapter(this, Android.Resource.Layout.SimpleSpinnerItem,
                    BookKeeperManager.Instance.db.GetExpenseAccounts().Select(ea => ea.AccountName + "( " + ea.AccountNumber + " )").ToList());

                expenseAdapter.SetDropDownViewResource(Android.Resource.Layout.
                SimpleSpinnerDropDownItem);
                type.Adapter = expenseAdapter;
                
                ArrayAdapter moneyAdapter =
                    new ArrayAdapter(this, Android.Resource.Layout.SimpleSpinnerItem,
                    BookKeeperManager.Instance.db.GetMoneyAccounts().Select(ea => ea.AccountName + "( " + ea.AccountNumber + " )").ToList());

                moneyAdapter.SetDropDownViewResource(Android.Resource.Layout.
                SimpleSpinnerDropDownItem);
                toFrom.Adapter = moneyAdapter;

                BookKeeperManager.Instance.db.SQLdb.Close();
    
                return "Expense";
            }
            
            return "Income";
        }

        private void setTaxAdapter()
        {
            ArrayAdapter taxAdapter =
                    new ArrayAdapter(this, Android.Resource.Layout.SimpleSpinnerItem,
                    BookKeeperManager.Instance.db.GetTaxes().Select(ta => ta.Tax).ToList());

            taxAdapter.SetDropDownViewResource(Android.Resource.Layout.
            SimpleSpinnerDropDownItem);
            tax.Adapter = taxAdapter;

        }
        private void TakePhoto()
        {
            int id = 0;
            AFile picDir = AEnvi.GetExternalStoragePublicDirectory(AEnvi.DirectoryPictures);
            AFile myDir = new File(picDir, "BookKeeperManager");
            
            if (!myDir.Exists())
            {
                myDir.Mkdirs();
            }

            AFile myFile = new File(myDir, "book" + id++ + ".jpg");
            myUri = AUri.FromFile(myFile);
            
            Intent i = new Intent(MediaStore.ActionImageCapture);
            i.PutExtra(MediaStore.ExtraOutput, myUri);
            StartActivityForResult(i, 0);
          }
        private void updateAmount()
        {
            if (total.Text != "")
            {
                totalCashEXTax = Convert.ToDouble(total.Text) - (Convert.ToDouble(total.Text) * ((Convert.ToDouble(tax.SelectedItem) / 100)));
                totalExk.Text = totalCashEXTax.ToString();
            }
            else
            {
                totalExk.Text = "-insert total";
            }
        }

        private void IsItEdit()
        {
            
            

            if (Intent.GetStringExtra("edit") == "yes")
            {
                int id = Convert.ToInt32(Intent.GetStringExtra("id"));
                Entry e = BookKeeperManager.Instance.db.GetEntry(id);
                BookKeeperManager.Instance.db.SQLdb.Close();    
                if (e.EntryType == "Income")
                {
                    incomeRB.Checked = true;
                    WhichRadioIsChecked();
                    switch (e.AccountType)
                    {
                        case "F�rs�ljning( 3000 )":
                            type.SetSelection(0);
                            System.Console.WriteLine("1");
                            break;
                        
                        case "F�rs�ljning av tj�nster( 3040 )":
                            System.Console.WriteLine("2");
                            type.SetSelection(1);
                            break;
                    }

                    switch (e.ToFromAccount)
                    {
                        case "Kassa( 1910 )":
                            System.Console.WriteLine("3");
                            toFrom.SetSelection(0);
                            break;

                        case "F�retagskonto( 1930 )":
                            toFrom.SetSelection(1);
                            System.Console.WriteLine("4");
                            break;
                        
                        case "Egna ins�ttningar( 2018 )":
                            toFrom.SetSelection(2);
                            System.Console.WriteLine("5");
                            break;
                    }

                }else
                {
                    expenseRB.Checked = true;
                    WhichRadioIsChecked();
                    switch (e.AccountType)
                    {
                        case "F�rbrukningsvaror och inventarier( 5400 )":
                            type.SetSelection(0);
                            System.Console.WriteLine("6");
                            break;

                        case "�vriga egna uttag( 2013 )":
                            type.SetSelection(1);
                            System.Console.WriteLine("7");
                            break;

                        case "Reklam och PR( 5900 )":
                            type.SetSelection(2);
                            System.Console.WriteLine("8");
                            break;
                    }
                    
                    switch (e.ToFromAccount)
                    {
                        case "Kassa( 1910 )":
                            System.Console.WriteLine("9");
                            toFrom.SetSelection(0);
                            break;

                        case "F�retagskonto( 1930 )":
                            toFrom.SetSelection(1);
                            System.Console.WriteLine("10");
                            break;
                    }

               }

                DateTime date = DateTime.ParseExact(e.Date, "yyyy'-'mm'-'dd", CultureInfo.InvariantCulture);
                datePick.UpdateDate(date.Year, date.Month, date.Day);
                
                des.Text = e.Description;
                total.Text = e.TotalWithTax.ToString();
                totalExk.Text = e.TotalExTax.ToString();
                switch (e.Tax)
                {
                    case "6.0%":
                        tax.SetSelection(0);
                        break;
                    
                    case "12.0%":
                        tax.SetSelection(1);
                        break;

                    case "25.0%":
                        tax.SetSelection(2);
                        break;
                }
                
                if (e.PhotoString != null)
                    {
                        photoView.SetAdjustViewBounds(true);
                        int height = Resources.DisplayMetrics.HeightPixels;
                        int width = photoView.Width;
                        var bitmap = ImageUtils.LoadAndScaleBitmap(e.PhotoString, width, height);
                        photoView.SetImageBitmap(bitmap);
                        bitmap.Dispose();
                        
                    }

                addEntrybutton.Text = "Uppdatera";
                
            }
        }
        
    }
}
